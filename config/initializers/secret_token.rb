# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Rubystore::Application.config.secret_key_base = '30c6b79d8394c0addfe099bdfc829c6f1e98fd83599a052834a72dc09d10425a504cd0e79a3ccdf8e5a059068b6bdb1aa1f73a9c1b6d59d04ac961577ad795b4'
